FROM adoptopenjdk/openjdk11:jre-11.0.7_10
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring
ARG JAR_FILE=target/*.jar
RUN mkdir /opt/app
EXPOSE 8080
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]
COPY ${JAR_FILE} /opt/app/app.jar
ENTRYPOINT ["java","-jar","/opt/app/app.jar"]

#docker run -p 8080:8080 -it --name chat --rm socket-project:latest
#--network="host"
#--net=host / --net="host"
#--add-host=host.docker.internal:host-gateway #since 20.04
#--add-host="localhost:192.168.59.3"
#docker build -t socket-project:latest .
#https://stackoverflow.com/questions/35960452/docker-compose-running-containers-in-nethost