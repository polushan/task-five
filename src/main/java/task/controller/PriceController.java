package task.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import task.domain.FinalPriceReceipt;
import task.domain.ShoppingCart;
import task.service.PriceService;

@RestController
@RequestMapping("/")
public class PriceController {
    private PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @PostMapping("/promo")
    public ResponseEntity promo() {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/receipt")
    public ResponseEntity receipt(@RequestBody ShoppingCart shoppingCart) {
        FinalPriceReceipt receipt = priceService.receipt(shoppingCart);
        return ResponseEntity.ok(receipt);
    }
}
