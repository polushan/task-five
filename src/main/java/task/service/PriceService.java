package task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import task.domain.FinalPricePosition;
import task.domain.FinalPriceReceipt;
import task.domain.ItemPosition;
import task.domain.ShoppingCart;
import task.domain.database.Item;
import task.domain.rule.ItemCountRule;
import task.domain.rule.ItemGroupRule;
import task.domain.rule.LoyaltyCardRule;
import task.repository.GroupRepository;
import task.repository.ItemsRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PriceService {
    private GroupRepository groupRepository;
    private ItemsRepository itemsRepository;

    @Autowired
    public PriceService(GroupRepository groupRepository, ItemsRepository itemsRepository) {
        this.groupRepository = groupRepository;
        this.itemsRepository = itemsRepository;
    }

    public void updatePriceRules(List<ItemCountRule> itemCountRules,
                                 List<ItemGroupRule> itemGroupRules,
                                 List<LoyaltyCardRule> loyaltyCardRules) {

    }

    public FinalPriceReceipt receipt(ShoppingCart shoppingCart) {
        List<ItemPosition> itemPositions = shoppingCart.getItemPositions();
        FinalPriceReceipt finalPriceReceipt = new FinalPriceReceipt();
        finalPriceReceipt.setDiscount(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_EVEN));

        List<FinalPricePosition> finalPricePositions = new ArrayList<>();
        BigDecimal total = BigDecimal.ZERO;
        if (!CollectionUtils.isEmpty(itemPositions)) {
            for (ItemPosition itemPosition : itemPositions) {
                Item item = itemsRepository.findById(itemPosition.getItemId())
                        .orElseThrow(() -> new IllegalStateException("Not found item: " + itemPosition.getItemId()));
                BigDecimal price = item.getPrice();
                total = total.add(price).setScale(2, RoundingMode.HALF_EVEN);
                FinalPricePosition finalPricePosition = new FinalPricePosition();
                finalPricePosition.setPrice(price);
                finalPricePosition.setRegularPrice(price);
                finalPricePosition.setName(item.getName());
                finalPricePosition.setId(item.getId());
                finalPricePositions.add(finalPricePosition);
            }
        }
        finalPriceReceipt.setFinalPricePositions(finalPricePositions);
        finalPriceReceipt.setTotal(total.setScale(2, RoundingMode.HALF_EVEN));
        return finalPriceReceipt;
    }
}
