package task.repository;

import org.springframework.data.repository.CrudRepository;
import task.domain.database.Group;

public interface GroupRepository extends CrudRepository<Group, String> {
}
