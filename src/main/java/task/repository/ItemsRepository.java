package task.repository;

import org.springframework.data.repository.CrudRepository;
import task.domain.database.Item;

public interface ItemsRepository extends CrudRepository<Item, String> {
}
