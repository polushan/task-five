package task.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinalPriceReceipt {
    private BigDecimal discount;
    private List<FinalPricePosition> finalPricePositions;
    private BigDecimal total;
}
