package task.domain.rule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemCountRule {
    private Integer bonusQuantity; // free
    private String itemId;
    private Integer shopId;
    private Integer triggerQuantity; // all
}
