package task.domain.rule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemGroupRule {
    private BigDecimal discount;
    private String groupId;
    private Integer shopId; //-1 for web
}
