package task.domain.rule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoyaltyCardRule {
    private BigDecimal number;
    private String shopId; // -1 for web
}
