package task.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCart {
    private Boolean loyaltyCard;
    private List<ItemPosition> itemPositions;
    private Integer shopId;
}
