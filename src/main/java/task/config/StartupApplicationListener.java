package task.config;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import task.csv.CsvUtil;
import task.domain.database.Group;
import task.domain.database.Item;
import task.repository.GroupRepository;
import task.repository.ItemsRepository;

import java.util.List;


@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
    @Value("classpath:csv/groups.csv")
    Resource groupsFile;
    @Value("classpath:csv/items.csv")
    Resource itemsFile;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    ItemsRepository itemsRepository;

    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<Group> groups = CsvUtil.read(Group.class, groupsFile.getInputStream());
        groupRepository.saveAll(groups);
        List<Item> items = CsvUtil.read(Item.class, itemsFile.getInputStream());
        itemsRepository.saveAll(items);
    }
}
