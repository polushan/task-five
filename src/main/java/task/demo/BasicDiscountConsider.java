package task.demo;

import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@EqualsAndHashCode
public class BasicDiscountConsider implements DiscountConsider {
    private static final String NAME = "basic";

    @Override
    public void consider(Check check) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        List<CheckLine> checkLines = check.getCheckLines();
        for (CheckLine checkLine : checkLines) {
            totalPrice = totalPrice.add(checkLine.getPriceForPosition());
        }
        totalPrice = totalPrice.setScale(2, RoundingMode.HALF_EVEN);
        check.setTotalPrice(totalPrice);
    }

    @Override
    public String getName() {
        return NAME;
    }
}
