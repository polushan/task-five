package task.demo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@EqualsAndHashCode
public class Check {
    private final List<CheckLine> checkLines;
    @Getter
    @Setter
    private BigDecimal totalPrice;

    public Check(List<ProductHolder> productHolders) {
        Objects.requireNonNull(productHolders);
        if (productHolders.isEmpty()) {
            this.checkLines = new ArrayList<>();
        } else {
            ProductHolderToCheckLineMapper checkLineMapper = new ProductHolderToCheckLineMapper();
            this.checkLines = productHolders.stream()
                    .map(checkLineMapper::mapToCheckLine)
                    .collect(Collectors.toList());
        }
    }

    public List<CheckLine> getCheckLines() {
        return Collections.unmodifiableList(checkLines);
    }


}
