package task.demo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class SecondFreeDiscountConsider implements DiscountConsider {
    private static final String NAME = "Second free";

    @Override
    public void consider(Check check) {
        List<CheckLine> checkLines = check.getCheckLines();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (CheckLine checkLine : checkLines) {
            BigDecimal amount = checkLine.getAmount();
            if (amount.compareTo(BigDecimal.ONE) <= 0) {
                totalPrice = totalPrice.add(checkLine.getPriceForPosition());
                continue;
            }
            int countOfFree = amount.intValue() / 2;
            BigDecimal priceForPosition = checkLine.getPriceForPosition();
            checkLine.setPriceForPosition(
                    priceForPosition
                    .subtract(checkLine.getPriceForOne()
                            .multiply(BigDecimal.valueOf(countOfFree)))
                    .setScale(2, RoundingMode.HALF_EVEN));
            checkLine.getDescription().append("Every second is free!");
            totalPrice = totalPrice.add(checkLine.getPriceForPosition());
        }
        totalPrice = totalPrice.setScale(2, RoundingMode.HALF_EVEN);
        check.setTotalPrice(totalPrice);
    }

    @Override
    public String getName() {
        return NAME;
    }
}
