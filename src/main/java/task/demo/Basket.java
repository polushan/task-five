package task.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Basket {
    private List<ProductHolder> products;

    public Basket(List<ProductHolder> products) {
        Objects.requireNonNull(products);
        this.products = new ArrayList<>(products);
    }

    public Basket() {
        this.products = new ArrayList<>();
    }

    public void addProduct(ProductHolder productHolder) {
        products.add(productHolder);
    }

    public List<ProductHolder> getProducts() {
        return Collections.unmodifiableList(products);
    }
}
