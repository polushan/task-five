package task.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CheckService {
    private List<DiscountConsider> discountConsiders;

    public CheckService(List<DiscountConsider> discountConsiders) {
        Objects.requireNonNull(discountConsiders);
        this.discountConsiders = new ArrayList<>(discountConsiders);
    }

    public Check generateCheck(Basket basket) {
        Objects.requireNonNull(basket);
        Check check = new Check(basket.getProducts());
        discountConsiders.forEach(discountConsider -> discountConsider.consider(check));
        return check;
    }

    public void addDiscountConsider(DiscountConsider discountConsider) {
        discountConsiders.add(discountConsider);
    }

    public void removeDiscountConsider(DiscountConsider discountConsider) {
        discountConsiders.remove(discountConsider);
    }
}
