package task.demo;

public interface DiscountConsider {
    void consider(Check check);

    String getName();
}
