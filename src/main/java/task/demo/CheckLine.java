package task.demo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Getter
@EqualsAndHashCode
public class CheckLine {
    private final Product product;
    private final BigDecimal amount;
    private final StringBuilder description;
    @Setter
    private BigDecimal priceForPosition;

    public CheckLine(Product product, BigDecimal amount) {
        Objects.requireNonNull(product);
        Objects.requireNonNull(amount);
        this.product = product;
        this.amount = amount;
        this.priceForPosition = amount.multiply(product.getPrice()).setScale(2, RoundingMode.HALF_EVEN);
        this.description = new StringBuilder();
    }

    public BigDecimal getPriceForOne() {
        return product.getPrice();
    }
}
