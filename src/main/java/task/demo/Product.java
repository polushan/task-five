package task.demo;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Objects;

@Getter
@EqualsAndHashCode
public class Product {
    private final String name;
    private final BigDecimal price;

    public Product(String name, BigDecimal price) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(price);
        this.name = name;
        this.price = price;
    }

    public ProductHolder amount(BigDecimal amount) {
        Objects.requireNonNull(amount);
        return new ProductHolder(this, amount);
    }
}
