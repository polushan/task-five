package task.demo;

public class ProductHolderToCheckLineMapper {
    public CheckLine mapToCheckLine(ProductHolder productHolder) {
        return new CheckLine(productHolder.getProduct(), productHolder.getAmount());
    }
}
