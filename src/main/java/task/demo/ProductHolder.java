package task.demo;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Objects;


@Getter
@EqualsAndHashCode
public class ProductHolder {
    private final Product product;
    private final BigDecimal amount;

    public ProductHolder(Product product, BigDecimal amount) {
        Objects.requireNonNull(product);
        Objects.requireNonNull(amount);
        this.product = product;
        this.amount = amount;
    }
}
