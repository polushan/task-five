CREATE TABLE groups (
   id VARCHAR (45),
   name VARCHAR (45),
   PRIMARY KEY (ID)
);

CREATE TABLE items (
   id VARCHAR (45),
   name VARCHAR (45),
   groupId VARCHAR (45),
   price decimal,
   PRIMARY KEY (ID)
);