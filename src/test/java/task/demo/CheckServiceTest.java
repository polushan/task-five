package task.demo;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckServiceTest {
    private CheckService underTest;

    @BeforeEach
    void setUp() {
        underTest = new CheckService(List.of());
    }

    @Test
    void generateCheck() {
        underTest.addDiscountConsider(new BasicDiscountConsider());
        ProductHolder meat = new Product("meat", BigDecimal.valueOf(50.5)).amount(BigDecimal.ONE);
        ProductHolder milk = new Product("meat", BigDecimal.valueOf(5.75)).amount(BigDecimal.valueOf(2));
        Basket basket = new Basket(List.of(meat, milk));
        Check check = underTest.generateCheck(basket);
        assertEquals(new BigDecimal("62.00"), check.getTotalPrice());
    }

    @Test
    void generateCheckSecondFree() {
        underTest.addDiscountConsider(new BasicDiscountConsider());
        underTest.addDiscountConsider(new SecondFreeDiscountConsider());
        ProductHolder meat = new Product("meat", BigDecimal.valueOf(50.5)).amount(BigDecimal.ONE);
        ProductHolder milk = new Product("meat", BigDecimal.valueOf(5.75)).amount(BigDecimal.valueOf(3));
        Basket basket = new Basket(List.of(meat, milk));
        Check check = underTest.generateCheck(basket);
        assertEquals(new BigDecimal("62.00"), check.getTotalPrice());
    }
}